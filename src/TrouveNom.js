import React, { Component } from 'react';

export default class TrouveNom extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null
    };
  }

  render() {
    return (
      <form onSubmit={this.trouveNom.bind(this)}>
        <input type="text" placeholder="trouveNom" ref="nomInput"/>
        <button>Trouve</button>
      </form>
    );
  }

  trouveNom(event) {
    event.preventDefault();

    const nomInput = this.refs.nomInput;
    const nom = nomInput.value;

    this.props.trouveNom(nom);
    this.refs.nomInput.value = '';
  }

}
